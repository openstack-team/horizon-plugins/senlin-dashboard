#!/usr/bin/make -f

include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=pybuild --with python3

override_dh_auto_clean:
	python3 setup.py clean
	rm -rf build .coverage *.egg-info
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_auto_build:
	echo "Do nothing..."

override_dh_auto_install:
	echo "Do nothing..."

override_dh_install:
	for i in $(PYTHON3S) ; do \
		python$$i setup.py install --install-layout=deb --root $(CURDIR)/debian/tmp ; \
	done

	# Move config files to standard location
	install -d -m 755 $(CURDIR)/debian/tmp/etc/openstack-dashboard/policy

	# Copy policy file to openstack-dashboard policy location
	cp $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/senlin_dashboard/conf/senlin_policy.json $(CURDIR)/debian/tmp/etc/openstack-dashboard/policy/senlin_policy.json

	# Copy config enabled to openstack-dashboard enabled location
	mkdir -p $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/local/enabled
	cp $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/senlin_dashboard/enabled/_[0-9]*.py $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/local/enabled

	dh_install
	dh_missing --fail-missing

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	NOSE_WITH_OPENSTACK=1 \
		NOSE_OPENSTACK_COLOR=1 \
		NOSE_OPENSTACK_RED=0.05 \
		NOSE_OPENSTACK_YELLOW=0.025 \
		NOSE_OPENSTACK_SHOW_ELAPSED=1 \
		DJANGO_SETTINGS_MODULE=senlindash_board.test.settings \
		python3-coverage run \
		$(CURDIR)/manage.py test senlin_dashboard --settings=senlin_dashboard.test.settings
	## Delete __pycache__
	find . -name __pycache__ -prune -exec rm -rf {} +;
endif
